package eu.catalyst.mbm.payload;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
public class MarketSessionPlain implements Serializable {
    private static final long serialVersionUID = 1L;


	private Integer id;

	private Date sessionStartTime;
	private Date sessionEndTime;
	private Date deliveryStartTime;

	private Date deliveryEndTime;

	private Integer sessionStatusid;

	private Integer marketplaceid;

	private Integer formid;

	private BigDecimal clearingPrice;
	//

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(Date sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public Date getSessionEndTime() {
		return sessionEndTime;
	}

	public void setSessionEndTime(Date sessionEndTime) {
		this.sessionEndTime = sessionEndTime;
	}

	public Date getDeliveryStartTime() {
		return deliveryStartTime;
	}

	public void setDeliveryStartTime(Date deliveryStartTime) {
		this.deliveryStartTime = deliveryStartTime;
	}

	public Date getDeliveryEndTime() {
		return deliveryEndTime;
	}

	public void setDeliveryEndTime(Date deliveryEndTime) {
		this.deliveryEndTime = deliveryEndTime;
	}

	public Integer getSessionStatusid() {
		return sessionStatusid;
	}

	public void setSessionStatusid(Integer sessionStatusid) {
		this.sessionStatusid = sessionStatusid;
	}

	public Integer getFormid() {
		return formid;
	}

	public void setFormid(Integer formid) {
		this.formid = formid;
	}

	public BigDecimal getClearingPrice() {
		return clearingPrice;
	}

	public void setClearingPrice(BigDecimal clearingPrice) {
		this.clearingPrice = clearingPrice;
	}

	public Integer getMarketplaceid() {
		return marketplaceid;
	}

	public void setMarketplaceid(Integer marketplaceid) {
		this.marketplaceid = marketplaceid;
	}

}
